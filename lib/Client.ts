
import { EventEmitter } from 'events';

export interface ClientOptions {}

export class Client extends EventEmitter {
	constructor(options?: ClientOptions) {
		super();
	}
}
