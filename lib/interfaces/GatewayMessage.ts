
import { GatewayOpCode } from '../constants';

export interface GatewayMessage {
	op: GatewayOpCode;
	d: any;
	t?: string;
}
