
import { GatewayOpCode } from '../../constants';
import { GatewayMessage } from '../GatewayMessage';

export interface GatewayHello extends GatewayMessage {
	op: GatewayOpCode.HELLO;
	d: {
		id: string;
		v: string;
		heartbeatInterval: number;
	};
	t: never;
}
