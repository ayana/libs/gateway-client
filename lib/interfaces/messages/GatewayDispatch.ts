
import { GatewayOpCode } from '../../constants';
import { GatewayMessage } from '../GatewayMessage';

export interface GatewayDispatch extends GatewayMessage {
	op: GatewayOpCode.DISPATCH;
	t: string;
}
