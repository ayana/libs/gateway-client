
export enum GatewayOpCode {
	DISPATCH = 0, // Events (server<->client)
	HEARTBEAT = 1, // Heartbeat (server<->client)
	IDENTIFY = 2, // Idenify Payload (server<-client)
	HELLO = 4, // Gateway Hello (server->client)
	HBACK = 5, // Gateway HBACK (server->client)
	CONNECTED = 6, // Gateway Connected (server->client)

	LOG = 3, // !!UNUSED!! Logs (server<-client)
}
