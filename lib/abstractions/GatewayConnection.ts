
import { EventEmitter } from 'events';
import { parse as URLParse } from 'url';
import * as WebSocket from 'ws';

import { GatewayOpCode } from '../constants';
import {
	GatewayDispatch,
	GatewayHello,
	GatewayMessage,
} from '../interfaces';

export interface ConnectData {
	secure?: boolean;
	host: string;
	port: number;
	path?: string;
	type?: string;
}

export enum ConnectionState {
	DISCONNECTED = 'disconnected',
	CONNECTING = 'connecting',
	CONNECTED = 'connected',
}

export interface SendMessageOptions {
	// request delivery comfirmation
	confirm?: boolean;

	// auto retry on delivery failure
	retry?: boolean;

	// max ammount of times to retry delivery
	maxRetries?: number;
}

export interface GatewayConnectionOptions {
	hello?(): { [key: string]: any };
}

export class GatewayConnection extends EventEmitter {
	public state: ConnectionState = ConnectionState.DISCONNECTED;
	public type: string = 'default';

	private socket: WebSocket;

	private options: GatewayConnectionOptions;

	constructor(options?: GatewayConnectionOptions) {
		super();

		this.options = Object.assign({}, {
			hello: () => ({}),
		} as GatewayConnectionOptions, options);
	}

	public async connect(connect: string | ConnectData) {
		if (typeof connect === 'string') {
			// we got a dsn, lets attempt to parse it
			const parse = URLParse(connect, true);

			connect = { host: null, port: 27532 };

			if (parse.hostname == null) throw new Error('Invalid Connect DSN');
			connect.host = parse.hostname;

			connect.port = parseInt(parse.port, 10) || connect.port;
			connect.path = parse.pathname;
		} else if (typeof connect !== 'object') throw new Error('DSN or ConnectData object required');

		// set defaults
		connect = Object.assign({}, {
			secure: true,
			path: '/',
			type: 'default',
		}, connect);

		this.type = connect.type || this.type;

		// create socket
		this.socket = new WebSocket(`ws${connect.secure == true ? '' : ''}://${connect.host}:${connect.port}${connect.path}`, {
			rejectUnauthorized: false,
		});
		this.state = ConnectionState.CONNECTING;

		// attach listeners
		this.socket.on('open', this.handleOpen.bind(this));
		this.socket.on('close', this.handleClose.bind(this));
		this.socket.on('error', this.handleError.bind(this));

		this.socket.on('message', this.handleMessage.bind(this));
	}

	public async disconnect() {
		// TODO
	}

	public sendMessage(message: string | GatewayMessage, options?: SendMessageOptions) {
		if (typeof message === 'object') {
			if (!message.op && message.op !== 0) throw new Error('Malformed Message: Must have valid OPCode.');
			if (!message.d && message.d !== null) throw new Error('Malformed Message: message data must be defined.');

			message = JSON.stringify(message);
		}

		options = Object.assign({}, {
			confirm: false,
			retry: true,
			maxRetries: 3,
		}, options);

		if (this.socket.readyState !== WebSocket.OPEN) {
			throw new Error('WebSocket readyState is not open.');
		}

		return new Promise<void>((resolve, reject) => {
			let attempt = 0;
			(function send() {
				this.socket.send(message, (e: Error) => {
					if (e) {
						if (options.retry && attempt++ < options.maxRetries) send.call(this);
						else reject(e);
						return;
					}
					resolve();
				});
			}).call(this);
		});
	}

	public close(code: number, data?: string) {
		this.socket.close(code, data);
	}

	private handleOpen() {
		this.emit('open');
	}

	private handleClose(code: number, data?: string) {
		this.emit('close', code, data);
	}

	private handleError(e: Error) {
		console.log(e);
	}

	private async handleMessage(data: WebSocket.Data) {
		const raw = data.toString();

		let message: GatewayMessage = null;
		try {
			message = JSON.parse(raw);
		} catch (e) {
			this.emit('warning', new Error('Server sent malformed JSON payload'));
			return;
		}

		// verify packet structure
		if (typeof message !== 'object') {
			this.emit('warning', new Error('Server sent malformed JSON payload'));
			return;
		}

		if (Object.values(GatewayOpCode).indexOf(message.op) === -1) {
			this.emit('warning', new Error('Server sent unknown OpCode'));
			return;
		}

		switch(message.op) {
			case GatewayOpCode.DISPATCH: {
				// verify that 't' exists
				if (typeof message.t !== 'string') {
					this.emit('warning', new Error('Server Dispatch message had no \'t\'ype'));
					return;
				}

				this.emit('dispatch', message as GatewayDispatch);
				break;
			}

			case GatewayOpCode.HELLO: {
				await this.handleHello(message as GatewayHello);
				break;
			}

			case GatewayOpCode.CONNECTED: {
				this.state = ConnectionState.CONNECTED;
				this.emit('connected');
				break;
			}

			default: // Not possible
		}
	}

	private async handleHello(message: GatewayHello) {
		// TODO: handle heartbeats and cache gateway id and version

		const identify: GatewayMessage = { op: GatewayOpCode.IDENTIFY, d: null };
		identify.d = Object.assign({}, { type: this.type }, this.options.hello());

		try {
			await this.sendMessage(identify);
		} catch (e) {
			this.emit('error', new Error('Failed to send identify'));
		}
	}
}
